<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostResource;
use App\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except'=>['index', 'show']]);
    }

    public function index()
    {
        return PostResource::collection(
            Post::all()
        );
    }

    public function show(Post $post)
    {
        return new PostResource($post);
    }
}
