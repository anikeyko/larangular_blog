import { Component, OnInit } from '@angular/core';
import {PostService} from '../../shared/services/post.service';
import {Post} from '../../shared/interfaces';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  posts: Post[] = [];

  constructor(
    private postService: PostService
  ) { }

  ngOnInit() {
    this.postService.getAll().subscribe(posts => {
      this.posts = posts;
    });
  }

}
