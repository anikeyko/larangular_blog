export interface User {
  email: string;
  password: string;
}

export interface LaravelAuthResponse {
  access_token: string;
  expires_in: string;
  token_type: string;
  user?: string;
}

export interface LaravelResource {
  data: string;
}


export interface Post {
  id?: number;
  title: string;
  content: string;
  created_at?: Date;
  author?: {
    name: string
  };
}

export interface PostData {
  data: Post;
}
