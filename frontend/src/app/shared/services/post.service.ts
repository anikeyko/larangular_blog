import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {LaravelResource, Post, PostData} from '../interfaces';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/posts`)
      .pipe(
        map((response: LaravelResource) => {
          return Object.keys(response.data).map(key => {
            return {
              ...response.data[key],
              created_at: new Date(response.data[key].created_at)
            };
          });
        })
      );
  }

  getById(id: number): Observable<any> {
    return this.http.get(`${environment.apiUrl}/posts/${id}`)
      .pipe(
        map((postData: PostData) => {
          const post = postData.data;
          return {
            ...post, id,
            date: new Date(post.created_at)
          };
        })
      );
  }

}
